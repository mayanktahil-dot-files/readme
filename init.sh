#!/bin/bash

export BRANCH=$(git rev-parse --abbrev-ref HEAD)

# Install Xcode CLI developer tools
# See http://apple.stackexchange.com/questions/107307/how-can-i-install-the-command-line-tools-completely-from-the-command-line

echo "Checking Xcode CLI tools"
# Only run if the tools are not installed yet
# To check that try to print the SDK path
xcode-select -p &> /dev/null
if [ $? -ne 0 ]; then
  echo "Xcode CLI tools not found. Installing them..."
  touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress;
  PROD=$(softwareupdate -l |
    grep "\*.*Command Line" |
    head -n 1 | awk -F"*" '{print $2}' |
    sed -e 's/^ *//' |
    tr -d '\n')
  softwareupdate -i "$PROD" -v;
else
  echo "Xcode CLI tools OK"
fi

# Install Homebrew
which -s brew
if [[ $? != 0 ]] ; then
    # Install Homebrew
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
    brew update
fi

# Install Iterm2 in default location
if [[ ! -d /Applications/iTerm.app ]]; then
    brew cask install iterm2
fi

# Install ZSH
brew install zsh

# Install Oh My ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Update OMZ with own .oh-my-zsh directory 
cd ~/.oh-my-zsh
git remote set-url origin https://gitlab.com/mayanktahil-dot-files/oh-my-zsh.git
git checkout $BRANCH
git pull origin $BRANCH
#git clone --single-branch --branch $BRANCH https://gitlab.com/mayanktahil-dot-files/oh-my-zsh.git

# Upgrade OMZ
upgrade_oh_my_zsh

# Install python 
brew install python

# Install pip
easy_install pip

# Install Powerline package for terminal emojis
pip install --user powerline-status

# Specify iterm2 preferences directory
defaults write com.googlecode.iterm2.plist PrefsCustomFolder -string "$(PWD)/iterm2"

# Tell iTerm2 to use the custom preferences in the directory
defaults write com.googlecode.iterm2.plist LoadPrefsFromCustomFolder -bool true