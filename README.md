# Init (Mac)

Below are manual steps for setting up your [iTerm2](https://www.iterm2.com/) terminal environment using [Homebrew](https://brew.sh/) and other command line interfaces. 

## Step 1 Install Homebrew

Before installing Homebrew, we need to install the CLI tools for Xcode. Open your terminal and run the command:

```
xcode-select —-install
```

> If you get an error, run `xcode-select -r` to reset `xcode-select`.

Then, install Homebrew.

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

```

## Step 2 Install iTerm2

To install iTerm2, run the command:

```
brew cask install iterm2
```

## Step 3 Install ZSH

By default, macOs ships with zsh located in `/bin/zsh` but we want to use zsh, installed via brew, and make iTerm2 use it.

```
brew install zsh
```

## Step 4 Install Oh My Zsh

OMZ runs on Zsh to provide cool features configurable within the `~/.zhrc` config file. Install Oh My Zsh by running the command:

```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

Check the installed version: 

```
zsh --version
```

Upgrade via: 

```
upgrade_oh_my_zsh
```

## Step 5 Configure ZSH

You can set your configurations in your `~/.zshrc`. To open the config file (`zshrc`), run the command:

```
nano ~/.zshrc
```



